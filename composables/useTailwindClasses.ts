const useTailwindClasses = () => {
	const utilityClasses = {
		afterTransition:
			"after:transition-all after:duration-300 after:ease-in-out",
		transition: "transition-all ease-in-out duration-300",
		container: "mx-auto md:w-10/12 w-full px-5",
		skeleton: "animate-pulse bg-gray-200 rounded-full",
		navbarMargin: "mt-16"
	}
	const classes = {
		navLink:
			"text-white relative after:bg-white after:absolute after:h-1 after:-bottom-1.5 after:left-0 after:w-0 after:rounded-full w-fit hover:after:w-full",
		clickedNavLink:
			"text-white relative after:bg-white after:absolute after:h-1 after:-bottom-1.5 after:left-0 after:w-full after:rounded-full w-fit",
	}
	return {
		...utilityClasses,
		...classes,
	}
}

export default useTailwindClasses
