const useBannerVisibility = () => {
	const isBannerVisible = useState("isBannerVisible", () => false)

	return {
		isBannerVisible,
	}
}

export default useBannerVisibility
