import type { IPostQuery } from "~/types/Post"

const useFetchApi = () => {
	const config = useRuntimeConfig()

	const getPostData = async ({ pageNumber, pageSize, sortBy }: IPostQuery) =>
		$fetch(`${config.public.apiUrl}/ideas`, {
			method: "GET",
			headers: {
				Accept: "application/json",
			},
			query: {
				"page[number]": pageNumber,
				"page[size]": pageSize,
				"append[]": ["small_image", "medium_image"],
				sort: sortBy,
			},
		})

	return {
		getPostData,
	}
}

export default useFetchApi
