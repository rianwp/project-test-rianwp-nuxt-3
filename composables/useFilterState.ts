import type { IFilter } from "~/types/Filter"

const useFilterState = () => {
	const filterState = useLocalStorage<IFilter>("filter", {
		sortBy: "-published_at",
		showPerPage: 10,
	})

	return {
		filterState,
	}
}

export default useFilterState
