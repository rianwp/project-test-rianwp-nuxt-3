import type { IPost } from "~/types/Post"

const usePostState = () => {
	const postState = useLocalStorage<IPost>("post", {
		posts: [],
		total: 0,
		currentPageNumber: 1,
	})

	return {
		postState,
	}
}

export default usePostState
