export type TSortBy = "-published_at" | "published_at"

export interface IFilter {
	sortBy: TSortBy
	showPerPage: number
}
