import type { TSortBy } from "./Filter"

export interface IPost {
	posts: IPostData[]
	total: number
	currentPageNumber: number
}

export interface IPostQuery {
	pageNumber: number
	pageSize: number
	sortBy: TSortBy
}

export type IPostData = {
	id: number
	medium_image: [
		{
			url: string
		}
	]
	small_image: [
		{
			url: string
		}
	]
	published_at: string
	title: string
}

export interface IPostResponse {
	data: IPostData[]
	meta: {
		total: number
	}
}
