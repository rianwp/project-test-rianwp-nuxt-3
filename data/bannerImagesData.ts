const bannerImagesData = {
	ideas: {
		url: "/img/bgimg.jpg",
		alt: "Ideas Banner",
	},
	about: {
		url: "/img/bgimg.jpg",
		alt: "About Banner",
	},
	careers: {
		url: "/img/bgimg.jpg",
		alt: "Careers Banner",
	},
	contact: {
		url: "/img/bgimg.jpg",
		alt: "Contact Banner",
	},
	services: {
		url: "/img/bgimg.jpg",
		alt: "Services Banner",
	},
	work: {
		url: "/img/bgimg.jpg",
		alt: "Work Banner",
	},
}

export default bannerImagesData
