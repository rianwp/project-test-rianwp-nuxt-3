const sortByData = [
	{
		name: "Newest",
		value: "-published_at",
	},
	{
		name: "Oldest",
		value: "published_at",
	},
]

export default sortByData