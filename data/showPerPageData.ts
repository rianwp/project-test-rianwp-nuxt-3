const showPerPageData = [
	{
		name: "10",
		value: 10,
	},
	{
		name: "20",
		value: 20,
	},
	{
		name: "50",
		value: 50,
	},
]

export default showPerPageData
