import showPerPageData from "./showPerPageData"
import sortByData from "./sortByData"

const filterData = [
	{
		label: "Show Per Page",
		name: "showPerPage",
		selectOptions: showPerPageData,
	},
	{
		label: "Sort By",
		name: "sortBy",
		selectOptions: sortByData,
	},
]

export default filterData
