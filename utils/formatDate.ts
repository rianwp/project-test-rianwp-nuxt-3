const formatDate = (date: Date) => {
	return date.toLocaleDateString("id-ID", { dateStyle: "long" })
}

export default formatDate
